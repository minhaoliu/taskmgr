# Taskmgr

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.0. Angular 4.0

# Login Page
![Screen_Shot_2020-10-06_at_6.20.01_PM](/uploads/22754df2593dc5b297f529f8370ad416/Screen_Shot_2020-10-06_at_6.20.01_PM.png)

# Project Detail
![Screen_Shot_2020-10-06_at_6.20.18_PM](/uploads/fc03cda357155d95c37c82eae59ff133/Screen_Shot_2020-10-06_at_6.20.18_PM.png)

# Project List & Add New Project
![Screen_Shot_2020-10-06_at_6.20.41_PM](/uploads/3675102ed53241d7d4b13873410817df/Screen_Shot_2020-10-06_at_6.20.41_PM.png)

# Edit Project
![Screen_Shot_2020-10-06_at_6.22.10_PM](/uploads/1149056e5429ad922642dc3b0a1da700/Screen_Shot_2020-10-06_at_6.22.10_PM.png)

# Side Menu
![Screen_Shot_2020-10-06_at_6.22.38_PM](/uploads/99be8eef3cb68f7dab3bb6d3cb774730/Screen_Shot_2020-10-06_at_6.22.38_PM.png)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
